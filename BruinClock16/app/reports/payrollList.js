﻿(function () {
    'use strict';
    var controllerId = 'payrollList';
    angular.module('app').controller(controllerId, ['common', employeeHours]);

    function employeeHours(common) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;
        vm.title = 'Payroll List';

        vm.employees = [
            { name: "Joe Bruin", id: 028174917, appt: 10, dist: 12, department: "Recruiting", description: "Student Asst" },
            { name: "Josephine Bruin", id: 453566271, appt: 12, dist: 11, department: "Marketing", description: "Office Asst" },
            { name: "Queen Latifah", id: 846326532, appt: 11, dist: 15, department: "Bus & Fin", description: "Select-A-Seat" },
            { name: "Tim Cook", id: 894278321, appt: 14, dist: 14, department: "Marketing", description: "Office Asst" },
            { name: "Joe Bruin", id: 028174917, appt: 10, dist: 12, department: "Recruiting", description: "Student Asst" },
            { name: "Josephine Bruin", id: 453566271, appt: 12, dist: 11, department: "Marketing", description: "Office Asst" },
            { name: "Queen Latifah", id: 846326532, appt: 11, dist: 15, department: "Bus & Fin", description: "Select-A-Seat" },
            { name: "Tim Cook", id: 894278321, appt: 14, dist: 14, department: "Marketing", description: "Office Asst" }
        ];

        activate();

        function activate() {
            common.activateController([], controllerId)
                .then(function () { log('Activated Payroll List View'); });
        }
    }
})();