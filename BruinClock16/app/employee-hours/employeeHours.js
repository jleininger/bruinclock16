﻿(function () {
    'use strict';
    var controllerId = 'employeeHours';
    angular.module('app').controller(controllerId, ['common', employeeHours]);

    function employeeHours(common) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;
        vm.title = 'Employee Hours';

        vm.employees = [
            { name: "Joe Bruin", id: 028174917 },
            { name: "Josephine Bruin", id: 453566271 },
            { name: "Queen Latifah", id: 846326532 },
            { name: "Tim Cook", id: 894278321 }
        ];

        activate();

        function activate() {
            common.activateController([], controllerId)
                .then(function () { log('Activated Employee Hours View'); });
        }
    }
})();